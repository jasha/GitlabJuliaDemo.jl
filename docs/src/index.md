# GitlabJuliaDemo documentation

This package is a minimal working example of setting up CI and documentation generation for Julia projects using [Gitlab](https://gitlab.com/).

The package repository is <https://gitlab.com/tkpapp/GitlabJuliaDemo.jl>.

This placeholder page demonstrates generated documentation.

```@docs
getA
```
